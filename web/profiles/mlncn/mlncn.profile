<?php

/**
 * @file
 * Profile for Intranet installation profile.
 */

use Drupal\Core\Config\ConfigException;
use Drupal\Core\Config\ConfigImporter;
use Drupal\Core\Config\FileStorage;
use Drupal\Core\Config\StorageComparer;
use Drush\Config\CoreExtensionFilter;
use Drush\Config\StorageWrapper;

/**
 * Implements hook_install_tasks_alter().
 */
function industrylabintranet_install_tasks_alter(&$tasks, $install_state) {
  $tasks['install_install_profile']['function'] = 'industrylabintranet_install_profile';
}

function industrylabintranet_install_profile(&$install_state) {
  $source_storage = new StorageWrapper(
    new FileStorage(config_get_config_directory(CONFIG_SYNC_DIRECTORY)),
    new CoreExtensionFilter(array('industrylabintranet')));
  $active_storage = Drupal::service('config.storage');

  Drupal::configFactory()
    ->getEditable('system.site')
    ->set('uuid', $source_storage->read('system.site')['uuid'])
    ->save();

  $storage_comparer = new StorageComparer($source_storage, $active_storage, Drupal::service('config.manager'));
  $config_importer = new ConfigImporter(
    $storage_comparer->createChangelist(),
    Drupal::service('event_dispatcher'),
    Drupal::service('config.manager'),
    Drupal::lock(),
    Drupal::service('config.typed'),
    Drupal::moduleHandler(),
    Drupal::service('module_installer'),
    Drupal::service('theme_handler'),
    Drupal::service('string_translation')
  );
  $config_importer->initialize();

  try {
    $config_importer->import();
  }
  catch (ConfigException $e) {
    foreach ($config_importer->getErrors() as $error) {
      drupal_set_message($error, 'error');
    }
  }

  drupal_flush_all_caches();
  install_install_profile($install_state);
}
