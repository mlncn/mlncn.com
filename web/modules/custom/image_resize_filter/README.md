Image Resize filter for Drupal 8
-------------------------------------------

Resizes images based on width and height attributes and optionally link to the
original image.

_NOTE_



The original plan is to provide two filters: one to creating an image derivate
and the other to link the derivate to the source (original) image. For now,
only the latter has been implemented.

### Installation

Install per normal https://www.drupal.org/documentation/install/modules-themes/modules-8.

### Credits

Initial development and maintenance by [Agaric](http://agaric.com/).
