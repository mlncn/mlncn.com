#!/usr/bin/env bash
export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get -y install \
	apache2 \
	git \
	mysql-server \
	php5-apcu \
	php5-cli \
	php5-curl \
	php5-fpm \
	php5-gd \
	php5-mysql \
	php5-xdebug \
	rake \
	vim-nox
apt-get -y autoremove

# Install composer.
if [ ! -f /usr/local/bin/composer ]; then
	curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
fi

cp -r /vagrant/provisioning/etc/* /etc/
chmod -R u+w /vagrant/web/sites/default
cp /vagrant/provisioning/settings.dev.php /vagrant/web/sites/default/settings.local.php
cp /vagrant/provisioning/services.yml /vagrant/web/sites/default/

php5enmod vagrant

# Create a local file folder inside the VM and symlink drupal's public file
# folder to updload files through the UI.
export FILES=/var/local/drupal
if [ ! -d $FILES ]; then
	mkdir -p $FILES
fi
chown -R www-data:staff $FILES
chmod -R g+w $FILES

if [ ! -L /vagrant/web/sites/default/files ]; then
	ln -s $FILES /vagrant/web/sites/default/files
fi

# Create the database.
if [ ! -d /var/lib/mysql/drupal ]; then
	mysqladmin -u root create drupal
fi

# Disable the system's default virtual host.
if [ -L /etc/apache2/sites-enabled/000-default.conf ]; then
	a2dissite 000-default
fi

# Enable the drupal website as the default virtual host.
if [ ! -L /etc/apache2/sites-enabled/drupal.conf ]; then
	a2ensite drupal
fi

# Enable the required web server modules
if [ ! -L /etc/apache2/mods-enabled/rewrite.load ]; then
	a2enmod rewrite
fi

if [ ! -L /etc/apache2/mods-enabled/proxy.load ]; then
	a2enmod proxy
fi

if [ ! -L /etc/apache2/mods-enabled/proxy_fcgi.load ]; then
	a2enmod proxy_fcgi
fi

service php5-fpm restart
service apache2 restart

# Add /vagrant/vendor/bin to PATH.
grep -q 'export PATH="/vagrant/vendor/bin:$PATH"' .bashrc

if [ $? -eq 1 ]; then
        echo 'export PATH="/vagrant/vendor/bin:$PATH"' >> /home/vagrant/.bashrc
fi
