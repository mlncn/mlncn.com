<?php

/**
 * @file
 * Settings for the dev environment.
 */

$databases['default']['default'] = array(
  'driver' => 'mysql',
  'database' => 'drupal',
  'username' => 'root',
  'password' => '',
  'host' => 'localhost',
  'prefix' => '',
);

$config_directories = array(
    CONFIG_SYNC_DIRECTORY => '../config/default'
);

$settings['install_profile'] = 'industrylabintranet';
$settings['hash_salt'] = 'mlncn8aAzK7QG5fVTgUsUD2VI08WbTWg9Lp2UM5hM5UjeDNeKHn0xfpRkmcAKYcSlhA5rxa0Rq';
$settings['trusted_host_patterns'] = array(
  '^mlncn\.local$',
);
